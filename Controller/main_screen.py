import importlib

import View.MainScreen.main_screen

# We have to manually reload the view module in order to apply the
# changes made to the code on a subsequent hot reload.
# If you no longer need a hot reload, you can delete this instruction.
importlib.reload(View.MainScreen.main_screen)


class MainScreenController:
    """
    The `MainScreenController` class represents a controller implementation.
    Coordinates work of the view with the model.
    The controller implements the strategy pattern. The controller connects to
    the view to control its actions.
    """

    def __init__(self, model):
        self.model = model  # Model.main_screen.MainScreenModel
        self.view = View.MainScreen.main_screen.MainScreenView(controller=self, model=self.model)

    def on_tap_button_number(self, value):
        self.model.number(value)

    def on_tap_button_plus(self, value):
        self.model.additive_operator_clicked(value)

    def on_tap_button_minus(self, value):
        self.model.additive_operator_clicked(value)

    def on_tap_button_times(self):
        pass

    def on_tap_button_divided(self):
        pass

    def on_tap_button_equal(self):
        self.model.equal_clicked()

    def on_tap_button_point(self):
        self.model.point_clicked()

    def on_tap_button_clear(self):
        self.model.clear()

    def get_view(self) -> View.MainScreen.main_screen:
        return self.view
