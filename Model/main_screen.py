from Model.base_model import BaseScreenModel

pendingMultiplicativeOperator = ""
pendingAdditiveOperator = ""


class MainScreenModel(BaseScreenModel):
    """
    Implements the logic of the
    :class:`~View.main_screen.MainScreen.MainScreenView` class.
    """

    def __init__(self):
        self._display = ""
        self.sumSoFar = 0.0
        self.factorSoFar = 0.0
        self.waitingForOperand = True
        self._observers = []

    def number(self, value):
        digitValue = int(value)
        if self._display == "0" and digitValue == 0.0:
            return

        if self.waitingForOperand:
            self._display=""
            self.waitingForOperand = False

        if self._display == "0" and digitValue != 0:
            self._display = str(digitValue)
        else:
            self._display = self._display + str(digitValue)
        self.notify_observers()

    def additive_operator_clicked(self, value):
        global pendingMultiplicativeOperator
        global pendingAdditiveOperator
        clickedOperator = value
        operand = float(self._display)

        if pendingMultiplicativeOperator != "":
            if self.calculate(operand, pendingMultiplicativeOperator) == False:
                self.abortOperation()
                return
            self._display = str(self.factorSoFar)
            operand = self.factorSoFar
            self.factorSoFar = 0.0
            pendingMultiplicativeOperator = ""

        if pendingAdditiveOperator != "":
            if self.calculate(operand, pendingAdditiveOperator) == False:
                self.abortOperation()
                return
            self._display = str(self.sumSoFar)
        else:
            self.sumSoFar = operand
        pendingAdditiveOperator = clickedOperator
        self.waitingForOperand = True
        self.notify_observers()

    def equal_clicked(self):
        global pendingMultiplicativeOperator
        global pendingAdditiveOperator
        operand = float(self._display)

        if pendingMultiplicativeOperator != "":
            if self.calculate(operand, pendingMultiplicativeOperator) == False:
                self.abortOperation()
                return
            operand = self.factorSoFar
            self.factorSoFar = 0.0
            pendingMultiplicativeOperator = ""

        if pendingAdditiveOperator != "":
            if self.calculate(operand, pendingAdditiveOperator) == False:
                self.abortOperation()
                return
            pendingAdditiveOperator = ""
        else:
            self.sumSoFar = operand

        self._display = str(self.sumSoFar)
        self.sumSoFar = 0.0
        self.waitingForOperand = True
        self.notify_observers()

    def point_clicked(self):
        if self.waitingForOperand:
            self._display = "0"
        if self._display.find(".") <= 0:
            self._display=self._display + "."

        self.waitingForOperand = False
        self.notify_observers()

    def abort_operation(self):
        pass

    def calculate(self,rigthOperand, pendingOperator):
        if pendingOperator == "+":
            self.sumSoFar += rigthOperand
        elif pendingOperator == U"\u2014":
            self.sumSoFar -= rigthOperand
        return True

    def clear(self):

        self._display = "0"
        self.waitingForOperand = True
        self.notify_observers()

    def clear_all(self):
        global pendingMultiplicativeOperator
        global pendingAdditiveOperator
        self.sumSoFar= 0.0
        self.factorSoFar=0.0
        pendingMultiplicativeOperator = ""
        pendingAdditiveOperator = ""
        self._display = "0"
        self.waitingForOperand = True
        self.notify_observers()

    def add_observer(self, observer):
        self._observers.append(observer)

    def remove_observer(self, observer):
        self._observers.remove(observer)

    def notify_observers(self):
        for x in self._observers:
            x.model_is_changed()
