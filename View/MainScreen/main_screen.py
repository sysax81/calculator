from View.base_screen import BaseScreenView
from kivy.properties import ObjectProperty

class MainScreenView(BaseScreenView):
    """"
    A class that implements the visual presentation `MyScreenModel`.
    """

    # <Controller.myscreen_controller.MyScreenController object>.
    controller = ObjectProperty()
    # <Model.myscreen.MyScreenModel object>.
    model = ObjectProperty()

    def __init__(self, **kw):
        super().__init__(**kw)
        self.model.add_observer(self)  # register the view as an observer    
        self.ids.display.text = "0"
    

        
    def model_is_changed(self) -> None:
        """
        Called whenever any change has occurred in the data model.
        The view in this method tracks these changes and updates the UI
        according to these changes.
        """
        self.ids.display.text = str(self.model._display)
